import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  books: any;
  books$:Observable<any>;
  userId:string;

  constructor(private bookservice:BooksService,public auth:AuthService) { }

  deletebook(id:string){
    this.bookservice.deleteBook(this.userId,id);
  }

  

  ngOnInit() {
   /* this.books = this.bookservice.getBooks().subscribe(
      (books) => this.books = books 
    );    */
    console.log("NgOnInit started")  
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
    this.books$ = this.bookservice.getBooks(this.userId);
   // this.bookservice.addBooks();
    }
    )
  }
}
