import { Injectable } from '@angular/core';
import {Observable, observable} from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import {  map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  bookCollection:AngularFirestoreCollection;

  //books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}] 

 // getBooks(){
  //  const booksObservable = new Observable(
  //  observer =>{
  //      setInterval (
  //        ()=> observer.next(this.books),5000
  //     )
  //    }
  //  )
  //  return booksObservable;
  //}
  getBooks(userId):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'});
    this.bookCollection = this.db.collection(`users/${userId}/books`);
        console.log('Books collection created');
        return this.bookCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

  /*getbook(id:string):Observable<any>{
    return this.db.doc(`books/${id}`).get();
  }*/

  getbook(userId, id:string):Observable<any>{
      return this.db.doc(`users/${userId}/books/${id}`).get();
  }

  addBooks(userId:string,title:string, author:string){
    const book = {title:title, author:author};
    //this.db.collection('books').add(book);
    this.userCollection.doc(userId).collection('books').add(book);
  }

  /*updateBook(id:string,title:string,author:string){
    const book = {title:title,author:author};
    this.db.doc(`books/${id}`).update(book);
  }*/
  updateBook(userId:string, id:string,title:string,author:string){
        this.db.doc(`users/${userId}/books/${id}`).update(
           {
             title:title,
             author:author
           }
         )
       }

  deleteBook(userId:string,id:string){
    //this.db.doc(`books/${id}`).delete();
    this.db.doc(`users/${userId}/books/${id}`).delete();
  }
/*  addBooks(){
    setInterval(
      () => this.books.push({title:"a new book",author:"new author"})
    ,5000)
  }
*/
  constructor(private db:AngularFirestore) { }
}
