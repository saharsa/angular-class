import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  constructor(private classify:ClassifyService,private router:Router) { }

  text:string; 

  onSubmit(){
    this.classify.doc = this.text;
    this.router.navigate(['/classified']);
  }

  ngOnInit() {
  }

}
