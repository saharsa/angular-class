import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  author:string;
  title:string;
  id:string;
  isedit:boolean = false;
  buttonText:string = "Add book" 
  userid:string;

  constructor(private bookservice:BooksService,public auth:AuthService,private router: Router,private route:ActivatedRoute) { }

  onSubmit(){
    if(this.isedit){
      this.bookservice.updateBook(this.userid,this.id,this.title,this.author);
      this.router.navigate(['/books'])
    }else{
      this.bookservice.addBooks(this.userid,this.title,this.author);
      this.router.navigate(['/books'])
    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.auth.user.subscribe(
      user =>{
        this.userid = user.uid;
        if(this.id){
          this.isedit =true;
          this.buttonText ="Update";
          this.bookservice.getbook(this.userid,this.id).subscribe(
            book=>{
              this.author = book.data().author;
              this.title = book.data().title;
              console.log(this.author);
            }
          )
        }
      }
    )
    //console.log(this.id);

  }

}
