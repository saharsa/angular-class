import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TempService } from '../temp.service';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  constructor(private route: ActivatedRoute,private tempService:TempService) { }

  temperature; 
  city;
  tempData$:Observable<Weather>;
  image:string;
  errorMessage:string;
  hasError:boolean;

  ngOnInit() {
    
    this.city= this.route.snapshot.params.city;
    this.tempData$ = this.tempService.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      },
      error=> {
        this.hasError = true;
        this.errorMessage = error.message;
        console.log('in the component '+ error.message);
      }
    )
  }
  

}
