import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  constructor(public classify:ClassifyService, private image:ImageService) { }

  category:string = "Loading.."
  categoryImage:string;

  ngOnInit() {
     this.classify.classify().subscribe(
      res => {
        this.category = this.classify.categories[res];
        this.categoryImage = this.image.images[res];
      } 
    );
  }

}
